#include "headers/ElementManager.h"

ElementManager elementManager = ElementManager();

/* Создание одной связи */
void link (int a, int b) {
    elementManager.link(a - 1, b - 1); //потому что массивы нумеруются с нуля
}

/* Создание элементов */
void init() {
    for(int i = 0; i < 18; i ++) {
        Element *element = new Element();
        element->setId(i);
        elementManager.add(element);
    }
}

/* Создание всех связей из графа */
void createLinks() {
    link(1, 2);
    link(1, 4);
    link(1, 6);
    link(2, 3);
    link(2, 5);
    link(2, 6);
    link(3, 18);
    link(4, 6);
    link(5, 9);
    link(6, 15);
    link(7, 8);
    link(7, 8);
    link(7, 9);
    link(7, 14);
    link(7, 15);
    link(8, 13);
    link(9, 12);
    link(10, 16);
    link(11, 12);
    link(11, 12);
    link(11, 12);
    link(11, 12);
    link(13, 14);
    link(14, 15);
    link(14, 18);
    link(14, 18);
    link(14, 18);
    link(15, 18);
    link(17, 18);
}

int main() {

    init();
    createLinks();
    return 0;
}
