#include "../headers/Element.h"

void Element::setId(int id) {
    this->id = id;
}

void Element::addLink(Element* e) {
    this->links.push_front(e);
}

std::list<Element*> Element::getLinks() {
    return this->links;
}

int Element::getId() {
    return this->id;
}
