#include "../headers/ElementManager.h"

void ElementManager::add(Element *e) {
    this->elements[pointer] = e;
    this->plate[pointer] = pointer;
    e->setId(pointer);
    pointer++;
}

Element* ElementManager::getElement(int id) {
    return this->elements[id];
}

void ElementManager::swap(int from, int to) {
    int temp = this->plate[from];
    this->plate[from] = this->plate[to];
    this->plate[to] = temp;
}

void ElementManager::link(int from, int to) {
    Element * a = elements[from];
    Element * b = elements[to];
    a->addLink(b);
    b->addLink(a);
}
