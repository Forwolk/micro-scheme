#ifndef MICROSCHEME_ELEMENT_H
#define MICROSCHEME_ELEMENT_H

#include <list>

class Element {
public:
    /* Получаем id (кэп, спасибо) */
    int getId();
    /* Назначаем id */
    void setId(int id);
    /* Добавляем связанный элемент */
    void addLink(Element* e);
    /* Получаем список указателей на все связанные элементы */
    std::list<Element*> getLinks();
private:
    /* Мы будем в менеджере обращаться к элементу по его порядковому номеру, т.е. по id */
    int id;
    /* Список связей с другими элементами */
    std::list<Element*> links;
};


#endif //MICROSCHEME_ELEMENT_H
