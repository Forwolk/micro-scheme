#ifndef MICROSCHEME_ELEMENTMANAGER_H
#define MICROSCHEME_ELEMENTMANAGER_H


#include "Element.h"
#include <list>

class ElementManager {
public:
    /* Добавляем элемент */
    void add(Element* e);
    /* Меняем местами элементы под указанными id */
    void swap(int from, int to);
    /* Получаем элемент по id */
    Element* getElement(int id);
    /* Создаем связь между элементами */
    void link (int from, int to);

private:
    /* Неизменяемый массив элементов. По id можно получить указатель на элемент. Грубо говоря словарик */
    Element *elements[18];
    /* Список id элементов. Этот порядок мы как раз меняем, чтобы все было хорошо */
    int plate[18];
    /* Этот указатель нам нужен лишь когда мы добавляем элементы в массив, чтобы присваивать уникальный id */
    int pointer;
};


#endif //MICROSCHEME_ELEMENTMANAGER_H
